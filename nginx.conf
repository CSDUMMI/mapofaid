events {}

http {
  client_max_body_size 2m;

  types {
    text/html                 html htm shtml;
    text/css                  css;
    application/x-javascript  js;

    application/pdf           pdf;
    application/zip           zip;
    application/x-tar         tar;
    application/gzip          gz;

    video/mp4                 mpg4 mp4;
    application/ogg           ogg;
    video/webm                webm;

    image/gif                 gif;
    image/jpeg                jpeg jpg;
    image/png                 png;

    text/plain                txt;
  }

  upstream app {
    server app:80;
  }

  server {
    listen 443 ssl;
    listen [::]:443 ssl;

    client_max_body_size 1m;
    server_name $hostname;

    ssl_certificate /cert.pem;
    ssl_certificate_key /privkey.pem;
    ssl_protocols TLSv1 TLSv1.1 TLSv1.2 TLSv1.3;
    ssl_ciphers HIGH:!aNULL:!MD5;


    gzip on;
    gzip_types "*";

    location /static/ {
      root /;
    }

    location /files/ {
      mp4;
      default_type text/plain;
      root /;
    }

    location = /favicon.ico {
      alias /static/favicon.ico;
    }

    location /graphql {
      proxy_pass http://app;
      proxy_set_header Host $http_host;
      proxy_redirect off;
    }

    location / {
	default_type text/html;
	alias static/index.html;
    }
  }

  server {
    listen 80;
    listen [::]:80;
    return 301 https://$host$request_uri;
  }
}
