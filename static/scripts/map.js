let map
let latitude = 53.64120
let longitude = 10.09714
let locations = {}

async function createMap(callback) {
  map = L.map("map");

  L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: 'pk.eyJ1IjoiY3NkdW1taSIsImEiOiJja3Jva2pqbzcyYjFjMm9vNTN2bDZmODk3In0.0fCrKm5LwvWkOBkkjU2k5g'
  }).addTo(map);
  map.setView([latitude, longitude], 13)

  map.on("locationfound", e => {
    console.log(e)
    latitude = e.latlng.lat
    longitude = e.latlng.lng

    map.setView(e.latlng, 13)

    callback(map, latitude, longitude)
  })

  map.locate()
}


function fillMap(callback) {
  async function inner(map, latitude, longitude) {
    let query = {
      query: `{
        near(latitude: ${latitude}, longitude: ${longitude}) {
          id
          longitude
          latitude
          name
          text
          user {
            name
            contact
            contact_type
          }
          program { name }
          services {
            name
          }
        }
      }`
    }

    let near = await fetch_gql(query)
    near.data.near.forEach(loc => {
      let marker = L.marker([loc.latitude,loc.longitude], {
        title: loc.name,
        alt: loc.text,
        riseOnHover: true
      })
      locations[loc.id] = locations[loc.id] == undefined ? {} : locations[loc.id]
      Object.assign(locations[loc.id], loc)

      marker.bindPopup(toPopup(loc))
      marker.addTo(map)
    })

    callback(map)
  }

  function toPopup(location) {
    let responsible = location.program ? location.program.name : location.user.name
    responsible = `<a href="${location.user.contact_type}:${location.user.contact}">${responsible}</a>`

    let popupText = `<h3>${location.name}</h3><p>${marked(location.text)}</p><p>${responsible}</p>`
    return DOMPurify.sanitize(popupText)
  }

  return inner
}

function addControls(callback) {
  function inner(map) {
    let search = L.DomUtil.create("input")

    L.Control.Search = L.Control.extend({
      onAdd: map => {
        search.type = "search"

        L.DomEvent.on(search, "change", fetchSearchResult)
        return search
      },

      onRemove: map => {
        L.DomEvent.off(search, "change", fetchSearchResult)
      }
    })
    L.Control.Watermark = L.Control.extend({
      onAdd: map => {
        let img = L.DomUtil.create("img")
        img.src = "https://upload.wikimedia.org/wikipedia/commons/f/fb/The_three_naval_training_ships%2C_Stosch%2C_Stein_and_Gneisenau_under_full_sail.png"
        img.style.width = "200px"

        return img
      },

      onRemove: map => {}
    })

    L.control.watermark = opts => {
      return new L.Control.Watermark(opts)
    }

    L.control.watermark({ position: "bottomleft" }).addTo(map)
    callback(map)
  }
  return inner
}

async function fetchSearchResult(e) {
  let search = e.target.value
  let query = {
    query: `{
      search(text: ${search}) {
        id
      }
    }`
  }

  let result = await fetch_gql(query)

  result.search.forEach(async l => {
    if(locations[l.id] == undefined) {
      locations[l.id] = await fetch_gql({
        query: `{
          location(id: ${l.id}) {
            longitude
            latitude
            name
            text
            user {
              name
              contact
              contact_type
            }
            program { name }
            services {
              name
            }
          }
        }`
      })
    }


  })
}

createMap(fillMap(addControls(map => {})))
