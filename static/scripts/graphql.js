async function fetch_gql(data) {
  return (await fetch(
    "/graphql",
    {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
        "Content-Length": data.length
      }
    }
  )).json();
}
