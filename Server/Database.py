"""
Database ORM definitions

Database Structure:

- Locations
- Programs
- Users

A Location is offered by
a program or user.

Programs are managed by users.

            User
           /   |
          /    |
      Program  |
         |     |
        Location

"""
import peewee as pw
import peeweedbevolve
from Crypto.Hash import SHA256
from Crypto.Random import get_random_bytes
import os
import uuid

DATABASE        = os.environ["POSTGRES_DB"]
PASSWORD        = os.environ["POSTGRES_PASSWORD"]
USER            = os.environ["POSTGRES_USER"]
HOST            = os.environ["POSTGRES_HOST"]

database = pw.PostgresqlDatabase( DATABASE
                                , password = PASSWORD
                                , user = USER
                                , host = HOST
                                )

class BaseModel(pw.Model):
    class Meta():
        database = database

    @classmethod
    def get_by_id_or_none(cls, pk):
        try:
            entry = cls.get_by_id(pk)
            return entry
        except pw.DoesNotExist:
            return None

# USER

class User(BaseModel):
    id = pw.UUIDField(primary_key = True, unique = True)
    name = pw.CharField(max_length = 500)

    # Hyperlinks: <contact_type>:<contact>, like mailto:info@example.org or tel:+123456789
    contact = pw.CharField(max_length = 500, unique = True)
    contact_type = pw.CharField(max_length = 50)

    password_hash = pw.CharField()
    salt = pw.FixedCharField(max_length = 64)
    reset_id = pw.FixedCharField(max_length = 64, null = True)

    is_privileged = pw.BooleanField(default = False)

    @classmethod
    def create_user(cls, name, password, contact, contact_type):
        salt = generate_salt()
        password_hash = hash_passwords(password, salt)
        user = cls.create(
            id = uuid.uuid4(),
            name = name,
            contact = contact,
            contact_type = contact_type,
            password_hash = password_hash,
            salt = salt,
            is_privileged = False
        )

        return user

    def authenticate(self, password : str) -> bool:
        password = hash_passwords(password, self.salt)
        return password == self.password_hash

    def password_change_request(self):
        """
        Password change process:
        1. Request password to change by providing contact field
        2. Send reset_id in link to contact (if contact type is supported)
        3. Contact follows link allowing to change the password.
        """
        self.reset_id = SHA256.new(data = get_random_bytes(2**4)).hexdigest()
        return self.reset_id

    def change_password(self, reset_id, password):
        """
        See password_change_requests
        """
        if self.reset_id == reset_id and self.reset_id is not None:
            self.salt = generate_salt()
            self.password_hash = hash_passwords(password, self.salt)
            return True
        else:
            return False

def generate_salt() -> bytes:
    return SHA256.new(data = get_random_bytes(2**4)).hexdigest()

def hash_passwords(password : str, salt : str) -> bytes:
    return SHA256.new(data = password.encode("utf-8") + salt.encode("utf-8")).hexdigest()


# PROGRAM

class Program(BaseModel):
    """
    A program may be implemented
    at one or more location.

    But it contains a concept, idea
    and name.
    """
    id = pw.UUIDField(primary_key = True)
    name = pw.CharField(max_length = 500)
    concept = pw.TextField()
    user = pw.ForeignKeyField(User, backref = "programs")

# LOCATION

class Location(BaseModel):
    """
    A Location
    """
    id = pw.UUIDField(primary_key = True)
    user = pw.ForeignKeyField(User, backref = "locations")
    program = pw.ForeignKeyField(Program, backref = "locations", null = True)

    name = pw.TextField()
    text = pw.TextField()

    latitude = pw.DoubleField()
    longitude = pw.DoubleField()
    calendar = pw.TextField() # ICS Calendar

class Service(BaseModel):
    id = pw.UUIDField(primary_key = True)
    name = pw.CharField()
    icon = pw.CharField()
    color = pw.CharField()

class LocationServices(BaseModel):
    """
    All Locations provide services
    """
    service = pw.ForeignKeyField(Service, backref = "locations")
    location = pw.ForeignKeyField(Location, backref = "services")

def create_tables():
    if "CREATE_TABLES" in os.environ:
        database.connect()
        database.evolve(interactive = False)
        database.close()

        return True
    else:
        return False

create_tables()
