from ariadne import ObjectType

location = ObjectType("Location")

@location.field("id")
def resolve_id(loc, info):
    return loc.id

@location.field("program")
def resolve_program(loc, info):
    return loc.program

@location.field("user")
def resolve_user(loc, info):
    return loc.user

@location.field("name")
def resolve_name(loc, info):
    return loc.name

@location.field("text")
def resolve_text(loc, info):
    return loc.text

@location.field("latitude")
def resolve_latitude(loc, info):
    return loc.latitude

@location.field("longitude")
def resolve_longitude(loc, info):
    return loc.longitude

@location.field("calendar")
def resolve_open(loc, info):
    return loc.calendar

@location.field("services")
def resolve_services(loc, info):
    return [s.service for s in loc.services]

service = ObjectType("Service")

@service.field("id")
def resolve_service_id(service, info):
    return service.id

@service.field("name")
def resolve_service_name(service, info):
    return service.name

@service.field("icon")
def resolve_service_icon(service, info):
    return service.icon

@service.field("color")
def resolve_service_color(service, info):
    return service.color

@service.field("locations")
def resolve_service_locations(service, info):
    return [l.location for l in service.locations]
