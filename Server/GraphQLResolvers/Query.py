from ariadne import ObjectType
from geopy import distance
import Server.Database as db
from ics import Calendar, Event
import arrow
from Server.GraphQLResolvers.auth import auth_required
from flask import session

query = ObjectType("Query")

@query.field("authenticated_user")
@auth_required
def resolve_authenticated_user(_, info):
    return db.User.get_by_id(session["user_id"])

@query.field("service")
def resolve_service(_, info, id):
    return db.Service.get_by_id_or_none(id)

@query.field("services")
def resolve_services(_, info):
    return db.Service.select()

@query.field("location")
def resolve_location(_, info, id):
    return db.Location.get_by_id_or_none(id)

@query.field("program")
def resolve_program(_, info, id):
    return db.Program.get_by_id_or_none(id)

@query.field("user")
def resolve_user(_, info, id):
    try:
        return db.User.get_by_id_or_none(id)
    except db.pw.DoesNotExist:
        return None

@query.field("near")
def resolve_near(_, info, longitude, latitude, radius, open_time):
    locations = db.Location.select()
    locs = []

    for location in locations:
        dist = distance.distance((latitude, longitude), (location.latitude, location.longitude)).km
        loc_services = [s.service for s in location.services]

        is_open = True
        if open_time is not None:
            calendar = Calendar(loc.calendar)

            is_open = False
            for event in calendar.on(arrow.get(open_time)):
                if event.name == "open":
                    is_open = True
                    break

        if radius >= dist and is_open:
            locs.append((location, dist))
    return map(lambda l: l[0], sorted(locs, key = lambda l: l[1]))
