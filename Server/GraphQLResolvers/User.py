from ariadne import ObjectType
import Server.Database as db

user = ObjectType("User")

@user.field("id")
def resolve_id(user, info):
    return user.id

@user.field("name")
def resolve_name(user, info):
    return user.name

@user.field("contact")
def resolve_contact(user, info):
    return user.contact

@user.field("contact_type")
def resolve_contact_type(user, info):
    return user.contact_type

@user.field("locations")
def resolve_locations(user, info):
    return user.locations

@user.field("programs")
def resolve_programs(user, info):
    return user.programs
