from ariadne import ObjectType

program = ObjectType("Program")

@program.field("name")
def resolve_name(program, info):
    return program.name

@program.field("concept")
def resolve_concept(program, info):
    return program.concept

@program.field("user")
def resolve_user(program, info):
    return program.user

@program.field("locations")
def resolve_locations(program, info):
    return program.locations
