from flask import session
import Server.Database as db

def auth_required(func):
    def inner(*args, **kwargs):
        if session.get("authenticated", default = False) == True:
            return func(*args, **kwargs)
        else:
            return None
    return inner

def get_user():
    return db.User.get_by_id(session["user_id"])
