from ariadne import MutationType
import uuid
from flask import session
import Server.Database as db
from Server.GraphQLResolvers.auth import auth_required
import Server.GraphQLResolvers.auth as auth

mutation = MutationType()

@mutation.field("create_user")
def resolve_create_user(_, info, name, password, contact, contact_type):
    return db.User.create_user(name, password, contact, contact_type)

@mutation.field("create_program")
@auth_required
def resolve_create_program(_, info, name, concept):
    user = db.User.get_by_id(session["user_id"])

    return db.Program.create(
                    id = uuid.uuid4(),
                    name = name,
                    concept = concept,
                    user = user
                )


@mutation.field("create_location")
@auth_required
def resolve_create_location(_, info, program, latitude, longitude, text, calendar, services):
    user = db.User.get_by_id(session["user_id"])
    program = db.Program.get_by_id(program)

    services = list(filter(lambda s: s is not None, [db.Service.get_by_id_or_none(s) for s in services]))

    if program.user == user and services == []:
        location = db.Location.create(
            id = uuid.uuid4(),
            program = program,
            user = user,
            text = text,
            latitude = latitude,
            longitude = longitude,
            calendar = calendar
        )

        for service in services:
            db.LocationServices.create(
                service = service,
                location = location
            )
        return location
    else:
        return None

@mutation.field("create_service")
@auth_required
def resolve_create_service(_, info, name, icon, color):
    user = db.User.get_by_id(session["user_id"])

    if user.is_privileged:
        return db.Service.create(
            id = uuid.uuid4(),
            name = name,
            icon = icon,
            color = color
        )
    else:
        return False

# Deletes
@mutation.field("delete_user")
@auth_required
def resolve_delete_user(_, info):
    user = db.User.get_by_id(session["user_id"])

    if len(user.organizations) == 0:
        user.delete_instance()
        return True
    else:
        return False

@mutation.field("delete_program")
@auth_required
def resolve_delete_program(_, info, program):
    user = db.User.get_by_id(session["user_id"])
    program = db.Program.get_by_id(program)

    if len(program.locations) == 0 and program.user == user:
        program.delete_instance()
        return True
    else:
        return False

@mutation.field("delete_location")
@auth_required
def resolve_delete_location(_, info, location):
    user = db.User.get_by_id(session["user_id"])
    location = db.Location.get_by_id(location)
    program = location.program
    org = program.organization

    for s in location.services:
        s.delete_instance()

    if org.user == user:
        location.delete_instance()
        return True
    else:
        return False

@mutation.field("delete_service")
@auth_required
def resolve_delete_service(_, info, service):
    user = db.User.get_by_id(session["user_id"])
    service = db.Service.get_by_id(service)

    if len(service.locations) == 0 and user.is_privileged:
        service.delete_instance()
        return True
    else:
        return False

# assign

@mutation.field("assign_program")
@auth_required
def resolve_assign_program(_, info, program, assignee_user):
    assigner_user = db.User.get_by_id(session["user_id"])
    program = db.Program.get_by_id(program)
    assignee_user = db.User.get_by_id(assignee_user)

    if program.user == assigner_user:
        program.user = assignee_user
        program.save()
        return program
    else:
        return program

@mutation.field("assign_location")
@auth_required
def resolve_assign_location(_, info, location, assignee_program):
    assigner_user = db.User.get_by_id(session["user_id"])
    location = db.Location.get_by_id(location)
    assignee_program = db.Program.get_by_id(assignee_program)

    if location.user == assigner_user:
        location.program = assignee_program
        location.save()

    return location

# change

def change_model(obj, new_obj, fields):
    for field in fields:
        new_value = getattr(new_obj, field)
        if new_value is not None:
            setattr(obj, field, new_value)

    if obj.is_dirty():
        obj.save()

    return obj

@mutation.field("change_user")
@auth_required
def resolve_change_user(_, info, new_user):
    user = db.User.get_by_id(session["user_id"])

    return change_model(user, new_user, ["name", "contact", "contact_type"])

@mutation.field("change_program")
@auth_required
def resolve_change_program(_, info, id, new_program):
    user = db.User.get_by_id(session["user_id"])
    program = db.Program.get_by_id(id)

    if program.user == user:
        return change_model(program, new_program, ["name", "concept"])

@mutation.field("change_location")
@auth_required
def resolve_change_location(_, info, id, new_location):
    user = auth.get_user()
    location = db.Location.get_by_id(id)

    if location.user == user:
        new_location["services"] = [db.Service.get_by_id_or_none(s) for s in new_location["services"]]
        new_location["services"] = [s for s in new_location["services"] if s is not None]

        return change_model(location, new_location, ["text", "latitude", "longitude", "calendar"])
    return location

@mutation.field("change_service")
@auth_required
def resolve_change_service(_, info, name, new_service):
    user = auth.get_user()

    if user.is_privileged:
        service = db.Service.get_by_id(name)

        return change_model(service, new_service, ["name", "icon", "color"])

@mutation.field("login")
def login(_, info, name, contact, contact_type, id, password):
    if id is None:
        users = db.User.select()

        if name is not None:
            users = users.where(db.User.name == name)

        if contact is not None:
            users = users.where(db.User.contact == contact)

        if contact_type is not None:
            users = users.where(db.User.contact_type == contact_type)

        if users.count() == 1:
            user = users.get()
        else:
            return False
    else:
        user = db.User.get_by_id(id)

    if user.authenticate(password):
        session["authenticated"] = True
        session["user_id"] = user.id

    return True
