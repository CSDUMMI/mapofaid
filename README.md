# MapOfAid

Map of Aid programs in Hamburg

## Purpose
Information is important to help people.
If you provide an aid service but nobody 
knows about it, you could shut it down.

## Setup
Install Docker and Docker-Compose

Create a signature and store
the certificate and private key 
in `cert.pem` and `privkey.pem`.

Using Certbot.

## Run
```bash
$ docker-compose up --build -d
```

This starts a service 
and listens on port 80 and 443.
(80 is a redirect to 443)

Go to https://localhost/ to view 
the homepage of the Services.

## Logs
```bash
$ docker-compose logs app
```
