FROM python:3.9

COPY . /app
WORKDIR /app
RUN pip install -r requirements.txt

EXPOSE 80
CMD gunicorn -b 0.0.0.0:80 --limit-request-line 0 --log-file - main:app
