"""
Implementing an
API for the Database
"""
from flask import Flask, request, session, jsonify
from ariadne import load_schema_from_path, graphql_sync, make_executable_schema
from ariadne.asgi import GraphQL
from ariadne.constants import PLAYGROUND_HTML
import os
import Server.Database as db
from Server.GraphQLResolvers import query, user, program, location, service, mutation


app = Flask(__name__)
app.secret_key = os.environ["SECRET_KEY"]
app.debug = "DEBUG" in os.environ

type_defs = load_schema_from_path("schema.graphql")
schema = make_executable_schema(type_defs, query, user, program, location, service, mutation)

@app.route("/graphql", methods = ["GET", "POST"])
def graphql():
    if request.method == "GET" and app.debug:
        return PLAYGROUND_HTML, 200
    else:
        data = request.get_json()

        success, result = graphql_sync(
            schema,
            data,
            context_value=request,
            debug=app.debug
        )

        status_code = 200 if success else 400
    return jsonify(result), status_code
